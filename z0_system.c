#include "z0_mfs.h"

void
z0_mfs_init(z0_mfs *mfs) {
	mfs->f_len = 0;
	mfs->f_cap = 8;
	mfs->g_len = 0;
	mfs->g_cap = 4;

	mfs->matrix = calloc(mfs->f_cap * mfs->g_cap / 8, sizeof (uint8_t));
	mfs->files  = malloc(sizeof (z0_file) * mfs->f_cap);
	mfs->groups = malloc(sizeof (z0_group) * mfs->g_cap);

	if (mfs->matrix == NULL ||
	    mfs->files	== NULL ||
	    mfs->groups == NULL) {
		fprintf(stderr, "ERR: could not allocate enough memory for the file system.");
		free(mfs->groups);
		free(mfs->files);
		free(mfs->matrix);
		exit(1);
	}
}

void
z0_mfs_connect(z0_mfs *mfs, uint32_t f_id, uint32_t g_id) {
	// rows -> file  ids.
	// cols -> group ids.
	uint8_t block;
	block = mfs->matrix[(f_id * mfs->g_cap + g_id) / 8];

	block |= ((0x1 << (7 - (f_id * mfs->g_cap + g_id) % 8)));
	mfs->matrix[(f_id * mfs->g_cap + g_id) / 8] = block;

	++mfs->groups[g_id].len;
}

void
z0_mfs_disconnect(z0_mfs *mfs, uint32_t f_id, uint32_t g_id) {
	// rows -> file  ids.
	// cols -> group ids.
	uint8_t block;
	block = mfs->matrix[(f_id * mfs->g_cap + g_id) / 8];

	block &= ~((0x1 << (7 - (f_id * mfs->g_cap + g_id) % 8)));
	mfs->matrix[(f_id * mfs->g_cap + g_id) / 8] = block;

	--mfs->groups[g_id].len;
}

void
z0_mfs_free(z0_mfs *mfs) {
	for (uint32_t i = 0; i < mfs->g_len; ++i)
		free((char*)((ptrdiff_t)mfs->groups[i].name & 0x0000FFFFFFFFFFFF));
	free(mfs->groups);
	for (uint32_t i = 0; i < mfs->f_len; ++i) {
		free((char*)((ptrdiff_t)mfs->files[i].name & 0x0000FFFFFFFFFFFF));
		if (mfs->files[i].size != 0)
			free(mfs->files[i].data);
	}
	free(mfs->files);
	free(mfs->matrix);
}

void
z0_mfs_print(z0_mfs *mfs) {
	printf("=============== MFS ===============\n");
	printf("FILE LENGTH:	%u\n", mfs->f_len);
	printf("FILE CAPACITY:	%u\n", mfs->f_cap);
	printf("GROUP LENGTH:	%u\n", mfs->g_len);
	printf("GROUP CAPACITY:	%u\n", mfs->g_cap);

	printf("---- MATRIX ----\n");
	uint32_t t = (mfs->g_cap) % 8;
	uint32_t i = 0;
	uint32_t o = 0;
	while (i < mfs->f_cap * mfs->g_cap / 8) {
		uint32_t col = mfs->g_cap;

		col = (col - 1) / 8;
		while (col > 0) {
			printf("%u ", (mfs->matrix[i] & 0b10000000) >> 0x7);
			printf("%u ", (mfs->matrix[i] & 0b01000000) >> 0x6);
			printf("%u ", (mfs->matrix[i] & 0b00100000) >> 0x5);
			printf("%u ", (mfs->matrix[i] & 0b00010000) >> 0x4);
			printf("%u ", (mfs->matrix[i] & 0b00001000) >> 0x3);
			printf("%u ", (mfs->matrix[i] & 0b00000100) >> 0x2);
			printf("%u ", (mfs->matrix[i] & 0b00000010) >> 0x1);
			printf("%u ", (mfs->matrix[i] & 0b00000001) >> 0x0);
	
			col = (col - 1) / 8;
			++i;
		}

		for (uint32_t x = 0; x < t; ++x)
			printf("%u ", (mfs->matrix[i] & ((0x1 << 7) >> (x + o))) >> (7 - (x + o)));
		printf("\n");

		o += t;
		t  = 8 - t;	
		if (o == 8) {
			++i;	
			o = 0;
		}
	}

	printf("\n---- FILES ----\n");
	for (uint32_t i = 0; i < mfs->f_len; ++i)
		z0_mfs_file_print(mfs, i);

	printf("---- GROUPS ----\n");
	for (uint32_t i = 0; i < mfs->g_len; ++i)
		z0_mfs_group_print(mfs, i);
}
