#ifndef Z0_MFS_H
#define Z0_MFS_H

#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct {
	char	*name;
	uint8_t *data;
	size_t	 size;
} z0_file;

typedef struct {
	char   *name;
	size_t	len;
} z0_group;

typedef struct {
	uint8_t *matrix;

	uint32_t f_len;
	uint32_t f_cap;
	z0_file *files;

	uint32_t  g_len;
	uint32_t  g_cap;
	z0_group *groups;
} z0_mfs;

void
z0_mfs_init(z0_mfs*);

uint32_t
z0_mfs_file_new(z0_mfs*, const char*, uint16_t);

void
z0_mfs_file_info(z0_mfs*, uint32_t, z0_file**);

uint32_t
z0_mfs_file_open(z0_mfs*, const char*, uint16_t);

void
z0_mfs_file_write(z0_mfs*, uint32_t, uint8_t*, size_t);

void
z0_mfs_file_write_off(z0_mfs*, uint32_t, uint8_t*, size_t, size_t);

void
z0_mfs_file_read(z0_mfs*, uint32_t, uint8_t*, size_t);

void
z0_mfs_file_read_off(z0_mfs*, uint32_t, uint8_t*, size_t, size_t);

void
z0_mfs_file_delete(z0_mfs*, uint32_t);

void
z0_mfs_file_print(z0_mfs*, uint32_t);

void
z0_mfs_file_print2(z0_file*);

uint32_t
z0_mfs_group_new(z0_mfs*, const char*, uint16_t);

void
z0_mfs_group_info(z0_mfs*, uint32_t, z0_group**);

void
z0_mfs_group_get_ids(z0_mfs*, uint32_t, uint32_t*);

void
z0_mfs_group_print(z0_mfs*, uint32_t);

void
z0_mfs_group_print2(z0_group*);

void
z0_mfs_connect(z0_mfs*, uint32_t, uint32_t);

void
z0_mfs_disconnect(z0_mfs*, uint32_t, uint32_t);

void
z0_mfs_free(z0_mfs*);

void
z0_mfs_print(z0_mfs*);

#ifdef Z0_MFS_REMOVE_NAMESPACE
#define file	z0_file
#define group	z0_group
#define mfs	z0_mfs

#define mfs_init		z0_mfs_init
#define mfs_file_new		z0_mfs_file_new
#define mfs_file_info		z0_mfs_file_info
#define mfs_file_open		z0_mfs_file_open
#define mfs_file_write		z0_mfs_file_write
#define mfs_file_write_off	z0_mfs_file_write_off
#define mfs_file_read		z0_mfs_file_read
#define mfs_file_read_off	z0_mfs_file_read_off
#define mfs_file_delete		z0_mfs_file_delete
#define mfs_file_print		z0_mfs_file_print
#define mfs_file_print2		z0_mfs_file_print2
#define mfs_group_new		z0_mfs_group_new
#define mfs_group_info		z0_mfs_group_info
#define mfs_group_get_ids	z0_mfs_group_get_ids
#define mfs_group_print		z0_mfs_group_print
#define mfs_group_print2	z0_mfs_group_print2
#define mfs_connect		z0_mfs_connect
#define mfs_disconnect		z0_mfs_disconnect
#define mfs_free		z0_mfs_free
#define	mfs_print		z0_mfs_print
#endif

#endif
