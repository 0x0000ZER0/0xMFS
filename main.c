#define Z0_MFS_REMOVE_NAMESPACE
#include "z0_mfs.h"

int
main() {
	z0_mfs mfs;
	z0_mfs_init(&mfs);

	uint32_t file_0;
	file_0 = z0_mfs_file_new(&mfs, "test.txt", sizeof ("test.txt") - 1);

	uint32_t file_1;
	file_1 = z0_mfs_file_new(&mfs, "img.jpg", sizeof ("img.jpg") - 1);

	uint32_t file_2;
	file_2 = mfs_file_new(&mfs, "img.jpeg", sizeof ("img.jpeg") - 1);

	uint32_t file_n;
	file_n = mfs_file_open(&mfs, "img.jpeg", sizeof ("img.jpeg") - 1);
	printf("FOUND A FILE: %u, (%u)\n", file_n, file_n == file_2);

	uint8_t file_data[] = "lorem ipsum";
	z0_mfs_file_write(&mfs, file_0, file_data, sizeof (file_data) - 1);

	file *file_info;
	z0_mfs_file_info(&mfs, file_0, &file_info);
	if (file_info != NULL)
		z0_mfs_file_print2(file_info);

	uint32_t group_0;
	group_0 = z0_mfs_group_new(&mfs, "home", sizeof ("home") - 1);

	z0_mfs_connect(&mfs, file_1, group_0);
	z0_mfs_connect(&mfs, file_2, group_0);
	z0_mfs_print(&mfs);

	z0_group *group;
	z0_mfs_group_info(&mfs, group_0, &group);
	if (group != NULL) {
		uint32_t *ids;
		ids = alloca(sizeof (uint32_t) * group->len);

		z0_mfs_group_get_ids(&mfs, group_0, ids);
		for (uint32_t i = 0; i < group->len; ++i)
			z0_mfs_file_print(&mfs, ids[i]);
	}

	z0_mfs_disconnect(&mfs, file_1, group_0);
	z0_mfs_print(&mfs);

	z0_mfs_free(&mfs);
	return 0;
}
