# 0xMFS

A matrix file system which is designed for better cache locality.

### Motivation

A regular file system uses tree like structure which on it's own is a linked list. Linked lists are performing very badly compared to arrays. Another advantage of matrix like structure is that SIMD can be use on the CPU side.

### API

- `Z0_MFS_REMOVE_NAMESPACE` macro is used for removing the `z0_` prefix.

```c
#define Z0_MFS_REMOVE_NAMESPACE
#include "z0_mfs.h"

// ...
// now instead of this,
z0_mfs_init(&mfs);
// we can write the following:
mfs_init(&mfs);
```

- Initializing the file system.

```c
z0_mfs mfs;
z0_mfs_init(&mfs);

// ...

z0_mfs_free(&mfs);
```

- For debugging purposes you can print the entire file system.

```c
z0_mfs_print(&mfs);
```

- `z0_mfs_file_new` function is used for creating a new file. It returns the **id** of the corresponding file. 

```c
uint32_t file_id;
file_id = z0_mfs_file_new(&mfs, "test.txt", sizeof ("test.txt") - 1);
```

- Similarly you can use the `z0_mfs_group_new` for creating a new group.

```c
uint32_t group_id;
group_id = z0_mfs_group_new(&mfs, "home", sizeof ("home") - 1);
```

- Opening an already existing file (in case of failure the function will return the capacity of files array in current file system):

```c
uint32_t file_n;
file_n = z0_mfs_file_open(&mfs, "img.jpeg", sizeof ("img.jpeg") - 1);

if (mfs->f_cap != file_n)
        printf("FOUND A FILE: %u\n", file_n);
```

- The following function can be used for writing data in the given file:

```c
 uint8_t file_data[] = "lorem ipsum";
 z0_mfs_file_write(&mfs, file_id, file_data, sizeof (file_data) - 1);
```

- To connect (disconnect) a file to (from) a group you can use following functions:

```c
z0_mfs_connect(&mfs, file_id, group_id);

// ...

z0_mfs_disconnect(&mfs, file_id, group_id);
```

### Structure

First of all let's define the unsigned integer set with the name $`U32`$ which we're going to use later on.

```math
U32 = \{ n \mid n \in \N_0 \text{ ,and } 0 \leqslant n \leqslant 2^{32} - 1 \}.
```

Now let's define the simplest object in our world, namely the *file* object, where $`m`$ is the meta data (this can include the name, size of the file) and the content which is denoted with $`\vec{b}`$ for vector of bytes.

```math
f = \langle m, \vec{b} \rangle
```

We can proceed with our definitions. The next one is so called **group**. This is a replacement of a folder. We'll see how is it different from a regular folder later on. For now let's define the **group** in the following way: a **group** can have (again) meta information which can have the letter $`m`$ and a count of files which will have the letter $`c`$.

```math
g = \langle m, c \rangle
```

Now that we defined our major objects, we can go ahead and see how our $`MFS`$ looks like. $`MFS`$ will contain 2 sets: one for all files ($`F`$) and the other for all groups ($`G`$).

```math
MFS = \langle F, G \rangle
```

Here is the definition of $`F`$ and $`G`$ :

```math
F = \{ f_0, f_1, ..., f_{i-1}, f_i \} \text{ ,where } i \in U32. \\
G = \{ g_0, g_1, ..., g_{j-1}, g_j \} \text{ ,where } j \in U32. \\
```

We can show the relationship between the files and group in the following way:

```math
M = \left[ \begin{array}{cccc}
a_{00} & a_{01} & \cdots & a_{0i} \\
a_{10} & a_{11} & \cdots & a_{1i} \\ 
\vdots & \vdots & \ddots & \vdots \\ 
a_{j0} & a_{j1} & \cdots & a_{ji} \\ 
 \end{array} \right]
```

where $`a`$ indicates whether the given $`f`$ is connected to that $`g`$ or not. With this design we'll have the following definition for $`a`$:

```math
a = \left\{ 
\begin{array}{ll}
1 \text{ , if } f \text{ is connected to } g \\
0 \text{ , if } f \text{ is not connected to } g
\end{array}
\right.
```
