SRC=main.c z0_system.c z0_file.c z0_group.c
OUT=mfs.out

all:
	gcc $(SRC) -O2 -DZ0_DEBUG -Wall -Wextra -fsanitize=leak -fsanitize=address -o $(OUT)

release:
	gcc $(SRC) -O2 -o $(OUT)

clear:
	rm $(OUT)
