#include "z0_mfs.h"

uint32_t
z0_mfs_group_new(z0_mfs     *mfs,
		 const char *name,
		 uint16_t    name_len) {
	if (mfs->g_len == mfs->f_cap) {
		mfs->g_cap *= 2;

		uint8_t *t_matrix;
		t_matrix = realloc(mfs->matrix, mfs->f_cap * mfs->g_cap / 8);

		z0_group *t_groups;
		t_groups = realloc(mfs->groups, sizeof (z0_group) * mfs->g_cap);

		if (t_matrix == NULL || t_groups == NULL) {
			fprintf(stderr, "ERR: could not allocate enough memory for files.");
			z0_mfs_free(mfs);
			exit(1);
		}

		mfs->matrix = t_matrix;
		mfs->groups = t_groups;
	}

	uint32_t id;
	id = mfs->g_len;

	mfs->groups[id].name = strdup(name);
	mfs->groups[id].len  = 0;

	ptrdiff_t ptr;
	ptr = (ptrdiff_t)mfs->groups[id].name | ((ptrdiff_t)name_len << (6 * 8));

	mfs->groups[id].name = (char*)ptr;

#ifdef Z0_DEBUG
	printf("DBG (name_len): %u\n", name_len);
	printf("DBG (name_ptr): %p\n", mfs->groups[id].name);
#endif

	++mfs->g_len;
	return id;
}

void
z0_mfs_group_info(z0_mfs *mfs, uint32_t id, z0_group **group) {
	if (id < mfs->g_len) {
		*group = &mfs->groups[id];	
	} else {
		*group = NULL;
	}
}

void
z0_mfs_group_get_ids(z0_mfs *mfs, uint32_t g_id, uint32_t *f_ids) {
	uint8_t block;
	uint8_t connected;
	
	size_t off;
	off = 0;

	for (uint32_t i = 0; i < mfs->f_len; ++i) {
		block = mfs->matrix[(i * mfs->g_cap + g_id) / 8];

		connected = block & ((0x1 << 7) >> ((i * mfs->g_cap + g_id) % 8));
		if (connected)
			f_ids[off++] = i;
	}	
} 

void
z0_mfs_group_print(z0_mfs *mfs, uint32_t id) {
	printf("GROUP ID:   %u\n", id);
	printf("GROUP NAME: \"%.*s\", size (%u)\n",
		(uint16_t)(((ptrdiff_t)mfs->groups[id].name & 0xFFFF000000000000) >> (6 * 8)),
		(char*)((ptrdiff_t)mfs->groups[id].name & 0xFFFFFFFFFFFF),
		(uint16_t)(((ptrdiff_t)mfs->groups[id].name & 0xFFFF000000000000) >> (6 * 8)));
	printf("GROUP LEN:  %lu\n", mfs->groups[id].len);

}

void
z0_mfs_group_print2(z0_group *group) {
	printf("GROUP NAME: \"%.*s\", size (%u)\n",
		(uint16_t)(((ptrdiff_t)group->name & 0xFFFF000000000000) >> (6 * 8)),
		(char*)((ptrdiff_t)group->name & 0xFFFFFFFFFFFF),
		(uint16_t)(((ptrdiff_t)group->name & 0xFFFF000000000000) >> (6 * 8)));
	printf("GROUP LEN:  %lu\n", group->len);
}
