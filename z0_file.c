#include "z0_mfs.h"

uint32_t
z0_mfs_file_new(z0_mfs	   *mfs,
		const char *name,
		uint16_t    name_len) {
	if (mfs->f_len == mfs->f_cap) {
		mfs->f_cap *= 2;

		uint8_t *t_matrix;
		t_matrix = realloc(mfs->matrix, mfs->f_cap * mfs->g_cap / 8);

		z0_file *t_files;
		t_files	= realloc(mfs->files, sizeof (z0_file) * mfs->f_cap);

		if (t_matrix == NULL || t_files == NULL) {
			fprintf(stderr, "ERR: could not allocate enough memory for files.");
			z0_mfs_free(mfs);
			exit(1);
		}

		mfs->matrix = t_matrix;
		mfs->files  = t_files;
	}

	uint32_t id;
	id = mfs->f_len;

	mfs->files[id].name = strdup(name);

	ptrdiff_t ptr;
	ptr = (ptrdiff_t)mfs->files[id].name | ((ptrdiff_t)name_len << (6 * 8));

	mfs->files[id].name = (char*)ptr;
	mfs->files[id].size = 0;

#ifdef Z0_DEBUG
	printf("DBG (name_len): %u\n", name_len);
	printf("DBG (name_ptr): %p\n", mfs->files[id].name);
#endif

	++mfs->f_len;
	return id;
}

void
z0_mfs_file_info(z0_mfs *mfs, uint32_t id, z0_file **file) {
	if (id < mfs->f_len) {
		*file = &mfs->files[id];	
	} else {
		*file = NULL;
	}
}

uint32_t
z0_mfs_file_open(z0_mfs     *mfs, 
		 const char *name,
		 uint16_t    name_len) {
	uint16_t  curr_len;
	char	 *curr_name;	

	for (uint32_t i = 0; i < mfs->f_len; ++i) {
		curr_len = ((ptrdiff_t)mfs->files[i].name & 0xFFFF000000000000) >> (6 * 8);
		if (curr_len != name_len)
			continue;

		curr_name = (char*)((ptrdiff_t)mfs->files[i].name & 0xFFFFFFFFFFFF);
		if (strncmp(curr_name, name, name_len) == 0)
			return i;
	}	

	return mfs->f_cap;
}

void
z0_mfs_file_write(z0_mfs   *mfs, 
		  uint32_t  id,
		  uint8_t  *data,
		  size_t    size) {
	z0_file *file;
	file = &mfs->files[id];

	file->size = size;
	file->data = malloc(size);
	if (file->data == NULL)
		return;

	memcpy(file->data, data, size);
}

void
z0_mfs_file_write_off(z0_mfs   *mfs, 
		      uint32_t  id,
		      uint8_t  *data,
		      size_t    size,
		      size_t	off) {
	z0_file *file;
	file = &mfs->files[id];

	memcpy(file->data + off, data, size);
}

void
z0_mfs_file_read(z0_mfs   *mfs, 
		 uint32_t  id, 
		 uint8_t  *data, 
		 size_t    size) {
	z0_file *file;
	file = &mfs->files[id];
		
	memcpy(data, file->data, size);
}

void
z0_mfs_file_read_off(z0_mfs   *mfs, 
		     uint32_t  id,
		     uint8_t  *data,
		     size_t    size,
		     size_t    off) {
	z0_file *file;
	file = &mfs->files[id];

	memcpy(data, file->data + off, size);
}

void
z0_mfs_file_delete(z0_mfs *mfs, uint32_t id) {
	// TODO: think of a way how to remove a file from
	// 	 the system efficiently, meaning array shifts
	// 	 should NOT happen. Probably using a flag will
	// 	 help here. Matrix and Vector checks 
	// 	 needs to be done as well.
	free((char*)((ptrdiff_t)mfs->files[id].name & 0xFFFFFFFFFFFF));
	free(mfs->files[id].data);	
}

void
z0_mfs_file_print(z0_mfs *mfs, uint32_t id) {
	printf("FILE ID:   %u\n", id);
	printf("FILE NAME: \"%.*s\", size (%u)\n", 
		(uint16_t)(((ptrdiff_t)mfs->files[id].name & 0xFFFF000000000000) >> (6 * 8)),
		(char*)((ptrdiff_t)mfs->files[id].name & 0xFFFFFFFFFFFF),
		(uint16_t)(((ptrdiff_t)mfs->files[id].name & 0xFFFF000000000000) >> (6 * 8)));
	printf("FILE SIZE: %zu\n", mfs->files[id].size);

	if (mfs->files[id].size != 0)
		printf("FILE DATA: %.*s\n", (int)mfs->files[id].size, mfs->files[id].data);
}

void
z0_mfs_file_print2(z0_file *file) {
	printf("FILE NAME: \"%.*s\", size (%u)\n", 
		(uint16_t)(((ptrdiff_t)file->name & 0xFFFF000000000000) >> (6 * 8)),
		(char*)((ptrdiff_t)file->name & 0xFFFFFFFFFFFF),
		(uint16_t)(((ptrdiff_t)file->name & 0xFFFF000000000000) >> (6 * 8)));
	printf("FILE SIZE: %zu\n", file->size);

	if (file->size != 0)
		printf("FILE DATA: %.*s\n", (int)file->size, file->data);
}
